﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using LearnToLearn.BindModels;
using LearnToLearn.Services;
using LearnToLearn.Helpers;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Controllers
{
    [AuthorizeRoles(Roles.Teacher)]
    public class CourseController : ApiController
    {
        private CourseService _courseService;

        public CourseController()
        {
            _courseService = new CourseService(new ModelStateWrapper(this.ModelState));
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            Course dbCourse = _courseService.GetById(id);

            if (!_courseService.Exists(dbCourse))
            {
                return NotFound();
            }

            CourseBindModel bindModel = new CourseBindModel(dbCourse);

            return Ok(bindModel);
        }

        [HttpGet]
        public IEnumerable<CourseBindModel> Get()
        {
            var dbCourses = _courseService.GetAll();
            List<CourseBindModel> courses = new List<CourseBindModel>();

            foreach (var dbCourse in dbCourses)
            {
                courses.Add(new CourseBindModel(dbCourse));
            }

            return courses;
        }

        [HttpPost]
        public IHttpActionResult Post(CourseCreateBindModel bindModel)
        {
            if (bindModel == null)
            {
                return BadRequest();
            }

            Course dbCourse = new Course
            {
                Name = bindModel.Name,
                TeacherId = bindModel.TeacherId,
                Capacity = bindModel.Capacity,
                Description = bindModel.Description,
                IsVisible = bindModel.IsVisible
            };

            if (!_courseService.AddOrUpdate(dbCourse))
            {
                return BadRequest(this.ModelState);
            }

            return Ok(bindModel);
        }
        
        [HttpPut]
        public IHttpActionResult Edit(int id, CourseEditBindModel bindModel)
        {
            Course dbCourse = _courseService.GetById(id);

            if (!_courseService.Exists(dbCourse))
            {
                return NotFound();
            }

            _courseService.OldCourseCapacity = dbCourse.Capacity;
            dbCourse.Name = bindModel.Name;
            dbCourse.Capacity = bindModel.Capacity;
            dbCourse.Description = bindModel.Description;
            dbCourse.UpdatedAt = DateTime.Now;
            dbCourse.IsVisible = bindModel.IsVisible;

            if (!_courseService.AddOrUpdate(dbCourse))
            {
                return BadRequest(this.ModelState);
            }

            return Ok(bindModel);
        }

        [HttpPatch]
        public IHttpActionResult Visibility(int id, CourseVisibilityBindModel bindModel)
        {
            Course dbCourse = _courseService.GetById(id);

            if (!_courseService.Exists(dbCourse))
            {
                return NotFound();
            }

            dbCourse.IsVisible = bindModel.IsVisible;

            if (!_courseService.AddOrUpdate(dbCourse))
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (!_courseService.DeleteById(id))
            {
                return InternalServerError();
            }

            return Ok();
        }
    }
}
