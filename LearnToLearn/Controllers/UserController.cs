﻿using System;
using System.Net;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using LearnToLearn.BindModels;
using LearnToLearn.Services;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Controllers
{
    [AllowAnonymous]
    public class UserController : ApiController
    {
        private UserService _userService;

        public UserController()
        {
            _userService = new UserService(new ModelStateWrapper(this.ModelState));
        }

        [HttpPost]
        public HttpResponseMessage Post(UserCreateBindModel bindModel)
        {
            if (bindModel == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            User user = new User
            {
                Email = bindModel.Email,
                Password = bindModel.Password,
                Name = bindModel.Name
            };
            
            if (!_userService.AddOrUpdate(user))
            {
                var error = new
                {
                    Error = ModelState.Values.SelectMany(e => e.Errors.Select(er => er.ErrorMessage))
                };

                return Request.CreateResponse(HttpStatusCode.Forbidden, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK, bindModel);
        }
    }
}
