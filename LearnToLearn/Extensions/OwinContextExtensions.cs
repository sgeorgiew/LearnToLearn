﻿using System.Linq;
using System.Security.Claims;
using Microsoft.Owin;

namespace LearnToLearn.Extensions
{
    public static class OwinContextExtensions
    {
        public static string GetUserId(this IOwinContext ctx)
        {
            string result = "-1";
            Claim claim = ctx.Authentication.User.Claims.FirstOrDefault(c => c.Type == "UserId");

            if (claim != null)
            {
                result = claim.Value;
            }

            return result;
        }
    }
}