﻿using System;
using System.ComponentModel.DataAnnotations;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.BindModels
{
    public class CourseEditBindModel
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int Capacity { get; set; }

        public bool IsVisible { get; set; }

        public CourseEditBindModel()
        { }

        public CourseEditBindModel(Course dbCourse)
        {
            Name = dbCourse.Name;
            Description = dbCourse.Description;
            Capacity = dbCourse.Capacity;
            IsVisible = dbCourse.IsVisible;
        }
    }
}