﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LearnToLearn.BindModels
{
    public class EnrollmentSetGradeBindModel
    {
        [Required]
        public int TeacherId { get; set; }

        [Range(2, 6)]
        public double Grade { get; set; }
    }
}