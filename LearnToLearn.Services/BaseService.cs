﻿using System;
using System.Linq;
using LearnToLearn.Data.Entities;
using LearnToLearn.Data.Repositories;

namespace LearnToLearn.Services
{
    public abstract class BaseService<TEntity, TRepository, TUnitOfWork>
        where TEntity : BaseEntity
        where TRepository : IBaseRepository<TEntity>
        where TUnitOfWork : IUnitOfWork
    {
        protected IValidationDictionary _validationDictionary;

        protected abstract TUnitOfWork _unitOfWork { get; set; }
        protected abstract TRepository _repository { get; set; }

        public BaseService(IValidationDictionary validationDictionary)
        {
            _validationDictionary = validationDictionary;
        }

        protected abstract bool OnAddValidate(TEntity itemToValidate);
        protected abstract bool OnUpdateValidate(TEntity itemToValidate);

        public bool Exists(TEntity itemToValidate)
        {
            if (itemToValidate == null)
            {
                return false;
            }

            return true;
        }

        public IQueryable<TEntity> GetAll(Func<TEntity, bool> filter = null)
        {
            return _repository.GetAll(filter);
        }

        public TEntity GetById(int id)
        {
            return _repository.GetById(id);
        }

        public bool DeleteById(int id)
        {
            _repository.DeleteById(id);
            return _unitOfWork.Save() > 0;
        }

        public bool AddOrUpdate(TEntity item)
        {
            // Validation Logic
            if (item.Id <= 0)
            {
                if (!OnAddValidate(item))
                {
                    return false;
                }
            }
            else
            {
                if (!OnUpdateValidate(item))
                {
                    return false;
                }
            }

            // Database Logic
            try
            {
                _repository.AddOrUpdate(item);
            }
            catch
            {
                _validationDictionary.AddError("unexpected_error", "Something gone wrong!");
                return false;
            }

            return _unitOfWork.Save() > 0;
        }
    }
}
