﻿using System;
using System.Linq;
using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories
{
    public interface IBaseRepository<T> : IDisposable where T : BaseEntity
    {
        T GetById(int id);

        IQueryable<T> GetAll(Func<T, bool> filter = null);

        void AddOrUpdate(T item);

        bool DeleteById(int id);
    }
}
