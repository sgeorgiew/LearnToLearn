﻿using LearnToLearn.Data.Entities;

namespace LearnToLearn.Data.Repositories
{
    public class EnrollmentRepository : BaseRepository<Enrollment>
    {
        public EnrollmentRepository(LearnToLearnContext context) : base(context)
        { }
    }
}
