﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnToLearn.Data.Entities
{
    public class Course : BaseEntity
    {
        [Required]
        [ForeignKey("Teacher")]
        public int TeacherId { get; set; }

        [Required]
        [StringLength(100)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public int Capacity { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        public virtual User Teacher { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }

        public Course()
        {
            DateTime dateTimeNow = DateTime.Now;

            CreatedAt = dateTimeNow;
            UpdatedAt = dateTimeNow;
        }
    }
}
